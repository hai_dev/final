import { Injectable } from "@angular/core";

import { Http } from '@angular/http';
import { AppConfig } from '../app/app.config';



@Injectable()
export class EventService {
    roomNumber: Number;
    isHappening: Boolean;
    public GetEventIDUrl = `${AppConfig.APIRoot}/api/events?roomNumber=509&isHappening=true`;
    public GetCheckListByIDUrl = ''
    constructor(private Http: Http){

    }

    getEventID(){
        // return new Promise((resovle, reject) => {
        //     let roomNumber = 509;
        //     let isHappening = true;
        //     this.http.get(`${EventService.GetEventIDUrl}?roomNumber=${roomNumber}&isHappening=${isHappening}`)
        //     .subscribe(rs => {
        //         console.log(rs);
        //         resovle(rs);
        //     });
        // });
            return this.Http.get(this.GetEventIDUrl)
            // .subscribe((rs)=> {
            //     console.log(rs);
            //     return rs;
            // }, err =>{
            //     console.log(err)
            // })
    }

    getCheckListByEventID(eventID) {
        let url = `${AppConfig.APIRoot}/api/events/${eventID}`;
        return this.Http.get(url);
    }
}

