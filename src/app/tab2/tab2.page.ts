import { Component, OnInit } from '@angular/core';
import { EventService } from '../../services/event.service';
import { Http } from '@angular/http';
import { ActivatedRoute } from "@angular/router";
import { AppConfig } from '../app.config';



@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page implements OnInit{
  eventID: String;
  Arrays: [];
  constructor(
    private eventService: EventService,
    private http: Http,
    private route: ActivatedRoute,
  ) {
  }
  ngOnInit() {
    this.getCheckList()
  }

  public getCheckList() {
    let data = localStorage.getItem('eventID');
    this.eventID = data;
    console.log(this.eventID);
    
    this.eventService.getCheckListByEventID(this.eventID)
    .subscribe(rs => {
      console.log(rs);
      let temp = JSON.parse(rs["_body"]);
      this.Arrays = temp.students;
      console.log(this.Arrays);
      return rs;
    }), err => {
      console.log(err);
    }
  }

  // public getCheckList() {
  //   this.eventService.getEventID()
  //   .subscribe((rs) => {
      
  //   })
  // }
}
