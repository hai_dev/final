import { Component, OnInit } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { File as File1 } from '@ionic-native/file/ngx';
import { Http, RequestOptions } from '@angular/http';
import { Base64 } from '@ionic-native/base64/ngx';
import { EventService } from '../../services/event.service';
import { NavController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';
import { Tab2Page } from '../tab2/tab2.page';
import { Router } from '@angular/router';
import { AppConfig } from '../app.config';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  title: String;
  roomNumber: String;
  endTime: any;
  startTime: any;
  myPhoto: any;
  eventID: String;
  constructor(
    private camera: Camera,
    private file: File1,
    private http: Http,
    private base64: Base64,
    private eventService: EventService,
    public navCtrl: NavController,
    private router: Router
  ) { 
  }

  ngOnInit() {
      this.getClassroomInfo();
      
  }

  public getClassroomInfo() {
    this.eventService.getEventID()
      .subscribe((rs) => {
        console.log(rs);
        let temp = JSON.parse(rs["_body"])
        this.roomNumber = temp[0].location;
        this.title = temp[0].title;
        let end =  new Date(temp[0].end);
        this.endTime = `${(end.getHours() < 10) ? ('0' + end.getHours()) : end.getHours()}:${(end.getMinutes() < 10) ? ('0' + end.getMinutes()) : end.getMinutes()}`;
        let start = new Date(temp[0].start);
        this.startTime = `${(start.getHours() < 10) ? ('0' + start.getHours()) : start.getHours()}:${(start.getMinutes() < 10) ? ('0' + start.getMinutes()) : start.getMinutes()}`;
        this.eventID = temp[0].eventId;
        localStorage.setItem('eventID', temp[0]._id);
        return rs;
      }, err => {
        console.log(err)
      })
  }

  public async passEventID(eventID) {
        let data = eventID;
        localStorage.setItem('id', JSON.stringify(data))
  }

  public takePicture() {
    // Create options for the Camera Dialog
    const sourceType = this.camera.PictureSourceType.CAMERA;
    const options = {
      quality: 100,
      sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      cameraDirection: 1,
      encodingType: 1,
    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      debugger
      this.uploadFile([imagePath]);
    }, () => {
      console.log('Error while selecting image.');
    });
  }

  uploadFile(results) {
    const galleryFormData = new FormData();
    const promises = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < results.length; i++) {
      const filePath = results[i];
      const tempPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
      const imageName = `${this.eventID}.png`;
      const encodeFilePromise = new Promise(async (resolve) => {
        try {
          const base64File = await this.base64.encodeFile(filePath);
          const blob = this.convertBase64ToBlob(base64File);
          // tslint:disable-next-line:variable-name
          const file_ = this.blobToFile(blob, imageName);
          galleryFormData.append('file', file_, imageName);
          resolve(true);

        } catch (err) {
          console.log(err)
        }

      });
      promises.push(encodeFilePromise);
    }

    Promise.all(promises)
      .then(() => {
        console.log(promises);
        this.uploadFileToSever(galleryFormData)
          .then(res => {
            const success = res.success;
            if (success) {
            }
            return res;
          });
      }).catch(() => { return; });
  }

  private blobToFile = (theBlob: Blob, fileName: string): File => {
    const b: any = theBlob;
    b.lastModifiedDate = new Date();
    b.name = fileName;
    return theBlob as File;
  }

  private convertBase64ToBlob(base64: string) {
    const info = this.getInfoFromBase64(base64);
    const sliceSize = 512;
    const byteCharacters = window.atob(info.rawBase64);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
      const byteNumbers = new Array(slice.length);

      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      byteArrays.push(new Uint8Array(byteNumbers));
    }

    return new Blob(byteArrays, { type: info.mime });
  }

  private getInfoFromBase64(base64: string) {
    const meta = base64.split(',')[0];
    const rawBase64 = base64.split(',')[1].replace(/\s/g, '');
    const mime = /:([^;]+);/.exec(meta)[1];
    const extension = /\/([^;]+);/.exec(meta)[1];

    return {
      mime,
      extension,
      meta,
      rawBase64
    };
  }

  async uploadFileToSever(formData: FormData): Promise<any> {
    const options = new RequestOptions();
    if (formData != null) {
      const url = `${AppConfig.APIRoot}/api/uploads/image`
      return this.http.post(url, formData, options)
        .toPromise()
        .then(res => res.json(), err => {
        });
    } else {
    }
  }
}